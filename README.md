# Тестовое задание от Mygento

### Задание №1
Есть xml файл (cart.xml) с следующим содержимым:
```xml
<?xml version="1.0" encoding="php "?>
<cart>
    <item>
        <sku>1111</sku>
        <qty>3</qty>
    </item>

    <item>
         <sku>2222</sku>
          <qty>1</qty>
    </item>
    <item>
        <sku>3333</sku>
        <qty>44</qty>
    </item>
</cart>
```
Необходимо, чтобы:
- Создавался cart.xml, если он не существует.
- При выполнении команды добавления товара “add [SKU] [QTY]” php файла (пример: php index.php add 1111 2) в xml файл записывалась информация об артикуле товара и его количестве (если товар есть – то увеличить кол-во, если товара нет – то добавить).
- При выполнении команды удаления товара “remove [SKU] [QTY]” обновлялась информация по удалению товара (аналогично п.2). Если кол-во товара <= 0, то нужно удалять соответствующую запись в XML.
- Скрипт может работать консольно или через POST запросы

**Результат:**
В src/xml.php

### Задание №3
Есть переменная $foo = 'москва'; Напишите команду в одну строчку для поднятия     в верхний регистр первой буквы слова (должно получиться Москва).

**Результат:**
```php
mb_convert_case($foo, MB_CASE_TITLE, "UTF-8"); // ucfirst() работать с кириллицей не будет
```

### Задание №4
Дана функция поиска пользователей по email. Опишите, пожалуйста, какие вы видите потенциальные проблемы данной функции.
```php
function searchUsersByEmail($email = '') { 
    global $limit; 
    $out = array(); 
    $query = 'SELECT * FROM users WHERE email LIKE ' . $email; 
    if ($res = @mysql_query($query)) { 
        while ($arr = @mysql_fetch_array($res)) { 
            $out[] = $arr; 
            if (count($out) > $limit) { 
                break; 
            } 
        } 
    } 
    return $out; 
}
```

**Потенциальные проблемы:**
- использование переменной из глобального скоупа (возможно, есть смысл передавать параметром - не понятно, нет контекста)
- использование mysql вместо mysqli
- использование оператора управления ошибками вместо их обработки
- лимит вообще можно указать в запросе
- LIKE без вайлдкарда, пусть оптимизатор, скорее всего, выполнит эту операцию за то же время, что и =

### Задание №5
Максимально сократите код следующей функции так, чтобы скорость её выполнения на большом числе запусков осталась неизменной или стала выше:
```php
function calcDelta(array $oldTags, array $newTags) {
    $addedTags = array();
    $removedTags = array();
    foreach ($oldTags as $tag) {
        if (!in_array($tag, $newTags)) {
            $removedTags[] = mb_strtolower($tag);
        }
    }
    foreach ($newTags as $tag) {
        if (!in_array($tag, $oldTags)) {
            $addedTags[] = mb_strtolower($tag);
        }
    }
    return array($addedTags, $removedTags);
}
```

**Результат:**
```php
function calcDelta(array $oldTags, array $newTags) {
    $addedTags = array_diff($newTags, $oldTags);
    foreach ($addedTags as &$tag) {
        $tag = mb_strtolower($tag);
    }
    $removedTags = array_diff($oldTags, $newTags);
    foreach ($removedTags as &$tag) {
        $tag = mb_strtolower($tag);
    }
    return array($addedTags, $removedTags);
}
```
### Задание №6
Напишите, пожалуйста, реализацию функции calc для представленного ниже кода
```php
$sum = function($a, $b)  { return $a + $b; };
calc(5)(3)(2)($sum);    // 10
calc(1)(2)($sum);       // 3
calc(2)(3)('pow');      // 8
```

**Результат:**
В src/calc.php
