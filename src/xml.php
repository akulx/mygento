<?php

function saveBasicXml(DOMDocument $xml) : void {
    $xml->formatOutput = true;
    $cart = $xml->createElement('cart');
    createItem($xml, $cart, '1111', '3');
    createItem($xml, $cart, '2222', '1');
    createItem($xml, $cart, '3333', '44');
    $xml->appendChild($cart);
    $cartxml = $xml->saveXML();
    $xml->save('cart.xml');
}

function createItem(DOMDocument $xml, DOMElement $cart, string $sku, string $qty) : void {
    $item = $xml->createElement("item");
    $cart->appendChild($item);
    $item->appendChild($xml->createElement("sku", $sku));
    $item->appendChild($xml->createElement("qty", $qty));
}

function modifyItem(SimpleXMLElement $xml, array $args) : void {
    $selected_arr = $xml->xpath("item[sku='$args[2]']");
    $selected = $selected_arr[0];
    if ($args[1] == 'add') {
        $selected->qty = $selected->qty + $args[3];
    } elseif ($args[1] == 'remove') {
        $selected->qty = $selected->qty - $args[3];
        if($selected->qty <= 0) {
            $cart = $xml->xpath('/cart');
            unset($cart[0]->item[$key]);
        }
    }
}

if (isset($_POST['mode'], $_POST['sku'], $_POST['qty'])) {
    $argv[1] = $_POST['mode'];
    $argv[2] = $_POST['sku'];
    $argv[3] = $_POST['qty'];
}

$xml = new DOMDocument('1.0', 'utf-8');
if (!file_exists('cart.xml')) {
    saveBasicXml($xml);
    if (!file_exists('cart.xml')) {
        throw new Exception('File was not created');
        exit;
    }
}

$nothing_to_remove = false;

$xml = simplexml_load_file('cart.xml');
$xmlItems = $xml->xpath('item');
foreach($xmlItems as $key => $xmlItem) {
    $items[$key] = (string)$xmlItem->sku;
}
$key = NULL;
$key = array_search($argv[2], $items);
if ($key !== NULL && $key !== false) {
    modifyItem($xml, $argv);
} else {
    if ($argv[1] == 'add') {
        $cart = $xml->xpath('/cart');
        $item = $cart[0]->addChild('item');
        $item->addChild('sku', $argv[2]);
        $item->addChild('qty', $argv[3]);
    } elseif ($argv[1] == 'remove') {
        throw new Exception('Nothing to remove');
        $nothing_to_remove = true;
    }
}
if (!$nothing_to_remove) {
    $xml->saveXML('cart.xml');
}
