<?php

function calc($in, $st = [])
{
    if (is_callable($in)) {
        return array_reduce(array_slice($st, 1), $in, $st[0]);
    }
    return function ($val) use ($st, $in) { 
        return calc($val, array_merge($st, [$in]));
    };
}
